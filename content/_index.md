+++
title = "Accueil"
+++

L'association Capoeiragem à Ramonville propose des cours de Capoeira avec le soutien pédagogique de [Capoeiragem Toulouse](https://www.capoeiragem.fr)

![affiche rentrée](./rentrée-ramonville-2024-2025.jpg)
