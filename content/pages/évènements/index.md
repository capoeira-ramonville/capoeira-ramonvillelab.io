+++
title = "Évènements"
date = 2023-08-01
+++

- 01/09/2024: Présence au [Forum des associations de Ramonville](https://www.ramonville.fr/actualites/forum-des-associations/) avec démonstration à 17h15

Les principaux évènements sont organisés avec les associations de [Capoeiragem](https://www.capoeiragem.fr/evenements)
 - [Festival enfants](http://www.capoeiragem.fr/evenements/festival-enfants)
 - [Festival adultes](http://www.capoeiragem.fr/evenements/festival-ados-adultes)
 - [Stages enfants](http://www.capoeiragem.fr/evenements/stages-enfants)
 - [Stages adultes](http://www.capoeiragem.fr/evenements/stages-ados-adultes)
 - [Marcevol](http://www.capoeiragem.fr/evenements/stage-d-ete-a-marcevol)
