+++
title = "Cours"
date = 2023-11-01
+++

Les cours ont lieu à la salle Pablo Picasso
- le lundi à 21h pour les adultes
- le mercredi à 17h15 pour les enfants de 7 à 12 ans
<iframe width="425" height="350" src="https://www.openstreetmap.org/export/embed.html?bbox=1.4748314023017886%2C43.55092368165536%2C1.4757809042930605%2C43.5526401773651&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><a class="is-size-7" href="https://www.openstreetmap.org/#map=19/43.55178/1.47531">Voir sur OpenStreetMap</a>
