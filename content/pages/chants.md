+++
title = "Chants"
date = 2023-09-01
+++

Cette pages sur les chants sera dédiée aux chants vus en cours pendant l'année. Un recueil de chants plus important se trouve [ici](https://www.capoeiragem.fr/capoeira/chansons)
