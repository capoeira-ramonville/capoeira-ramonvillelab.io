+++
title = "Inscription"
date = 2023-10-01
+++

L'année de Capoeira commencera à Ramonville le 16 septembre.

Vous êtes les bienvenus pour venir faire un ou deux cours d'essais.

Ensuite l'inscription se fait avec la fiche d'incription suivante vous correspondant:

- [Pour les enfants](enfants-ramonville-2024-2025.pdf)
- [Pour les adultes](adultes-ramonville-2024-2025.pdf)
